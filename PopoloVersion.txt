COMANDI PER CONFIGURAZIONE INIZIALE

git init: inizializza un repository Git, creando una cartella .git dove Git archivia tutti i dati.
git status: mostra lo stato attuale del repository, utile da usare all'inizio e periodicamente per verificare le modifiche.
git add NomeFile.md: aggiunge un file specifico all'area di staging.
git commit -m 'commento': registra le modifiche nell'area di staging con un messaggio di commento.
git log: visualizza la cronologia dei commit, mostrando i messaggi associati a ciascun commit.
git blame: identifica l'autore delle ultime modifiche a ogni riga di un file, utile per analizzare la cronologia delle modifiche.


VEDERE PASSWORD E NOME ASSEGNATI

vim .git/config: apre il file di configurazione Git per vedere le impostazioni.
vim git: comanda per vedere le impostazioni di Git (forse un errore, dovrebbe essere vim .git/config).


COMANDI DI LETTURA

git log: visualizza la cronologia dei commit.
git log --oneline: mostra la cronologia dei commit in formato sintetico.
git diff: mostra le differenze tra le modifiche non committate e l'ultimo commit.
git show: visualizza i dettagli dell'ultimo commit.
git show <id>: visualizza i dettagli di un commit specifico.
git diff <id1> <id2>: mostra le differenze tra due commit specifici.


PER ELIMINARE UN FILE

git rm nomeFile: rimuove un file e richiede un commit per completare l'operazione.
git commit -m 'commento': necessario per confermare la rimozione del file.
git status: verifica che l'operazione sia stata eseguita correttamente.
git rm --cached nomeFile: rimuove un file dall'indicizzazione di Git senza eliminarlo dal disco.
git restore --staged <NOME FILE>: annulla l'aggiunta di un file all'area di staging.


PER IGNORARE UN FILE O UNA DIRECTORY A GIT

touch .gitignore: crea un file .gitignore per specificare i file da ignorare.
vim .gitignore: apre il file .gitignore per l'editing.
git add .gitignore: aggiunge il file .gitignore all'area di staging.
git commit -m 'commento': registra il file .gitignore nel repository.


COMANDI PER CONFIGURAZIONE NOME E EMAIL

git config user.name "nome": imposta il nome dell'utente per il repository corrente.
git config user.email "email": imposta l'email dell'utente per il repository corrente.
git config --global: applica le impostazioni di nome e email a tutti i repository.


PER SPOSTARE/RINOMINARE UN FILE

mv nomeFile nuovoNomeFile: rinomina un file.
git add .: aggiunge tutte le modifiche all'area di staging.
git commit -m 'commento': registra le modifiche nel repository.
git mv nomeFile nuovoNomeFile: sposta o rinomina un file e tiene traccia delle modifiche senza bisogno di git add.


CREAZIONE E GESTIONE BRANCH

git branch: elenca i branch esistenti.
git merge: unisce due branch.
git checkout -b NomeBranch: crea e passa a un nuovo branch.
git checkout master: torna al branch principale.
git checkout <id>: passa a un punto specifico nella cronologia.
git branch -d NomeBranch: elimina un branch.
git branch -D NomeBranch: forza l'eliminazione di un branch remoto.
ls: mostra lo stato del repository prima del push.
git push origin main: invia le modifiche al repository remoto.

COMANDI AGGIUNTIVI "SCAMPOLI"

git commit --amend: modifica l'ultimo commit.
git commit --sign: verifica crittograficamente l'autore dei commit.
git add -p: aggiunge porzioni di modifiche all'area di staging.
git reset: rimuove i commit mantenendo le modifiche ai file.
git revert: annulla un commit specifico creando un commit opposto.
git cherry-pick: applica un commit specifico da un branch a un altro.
git clone: crea una copia locale di un repository esistente.
git tag: associa una versione testuale a un punto del codice, utilizzando il versioning semantico.
git stash: salva temporaneamente le modifiche non committate.

